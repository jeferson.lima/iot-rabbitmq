# Get Bus Fare Service

from nameko.rpc import rpc, RpcProxy

class bus_fare:
    name = "ticket_value"

    @rpc
    def get_value(self, ):
        return {'passagem':{'dinheiro':2.2,
                'estudante':1.2,
                'passe':3.3,
                'idoso': 1.0,
                'idoso_2': 0,
                'necessidades_especiais': 1.65,
                'necessidades_especiais_2': 1.65}}