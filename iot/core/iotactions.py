# IoT-RabbitMQ API Packege

import logging
import numpy as np
from ..core.stdMessage import IoTMsg
from ..bus.busTicket import TicketValue
from ..gps.Gps_Position import Gps_Device_Position

logger = logging.getLogger(__name__)


class Actions(IoTMsg):
    """
    Parameters
    ----------
    Returns
    -------
    Raises
    ------
    """

    def __init__(self, header, body):
        super().__init__()
        self.header = header
        self.body = body


    def select_actions(self):
        try:
            action = self.header['action'].split('.')
            _action = action[2]
            # check it is the action format
            assert action[0] == "iotrabbitmq" and action[1] == "api" \
                                and _action in self.actions_list, "not a valid action"

            if _action == "ticket_value":
                try:
                    values = TicketValue()
                    res = values.get_ticket_value()
                    self.body.update(res)
                    # everything ok
                except:
                    self.header.update({"code": "004"})
            elif _action == "device_gps_pos":
                try:
                    pos = Gps_Device_Position(self.body)
                    res = pos.get_device_gps_pos()
                    self.body.update(res)
                except:
                    self.header.update({"code": "005"})
            elif _action == "bus_info":
                try:
                    pos = Gps_Device_Position(self.body)
                    res = pos.get_device_gps_pos()
                    self.body.update(res)
                except:
                    self.header.update({"code": "006"})
            else:
                # not a valid action
                self.header.update({"code": "003"})
        except:
            # ticket value error
            self.header.update({"code":"002"})
        msg = {"header": self.header, "body": self.body}
        return msg