# IoT-RabbitMQ API Packege

import numpy as np
from ..core.stdMessage import IoTMsg

class TicketValue(IoTMsg):
    """
    Parameters
    ----------
    Returns
    -------
    Raises
    ------
    """

    def __init__(self):
        super().__init__()
    
    def get_ticket_value(self):
        with self.services as rpc:
            value = rpc.ticket_value.get_value()
        return value